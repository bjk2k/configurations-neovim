" Plugin specification and lua stuff
lua require('plugins_init')

"o-------------------------------------
"| vim-sandwich
"o-------------------------------------

" unmap s to avoid conflict 
nmap s <Nop>
omap s <Nop>

if exists('g:vscode')
  " VSCode extension
  highlight OperatorSandwichBuns guifg='#aa91a0' gui=underline ctermfg=172 cterm=underline
  highlight OperatorSandwichChange guifg='#edc41f' gui=underline ctermfg='yellow' cterm=underline
  highlight OperatorSandwichAdd guibg='#b1fa87' gui=none ctermbg='green' cterm=none
  highlight OperatorSandwichDelete guibg='#cf5963' gui=none ctermbg='red' cterm=none
else
  " ordinary Neovim
endif

"o--------------------------------------
"| vista
"o--------------------------------------

if exists('g:vscode')
else
  let g:vista#renderer#icons = {
        \ 'member': '',
        \ }

  " Do not echo message on command line
  let g:vista_echo_cursor = 0
  " Stay in current window when vista window is opened
  let g:vista_stay_on_open = 0

  nnoremap <silent> <Space>t :<C-U>Vista!!<CR>
endif

"o--------------------------------------
"| ultisnips
"o--------------------------------------

if exists('g:vscode')
else
  " Trigger configuration. Do not use <tab> if you use YouCompleteMe
  let g:UltiSnipsExpandTrigger='<c-j>'

  " Do not look for SnipMate snippets
  let g:UltiSnipsEnableSnipMate = 0

  " Shortcut to jump forward and backward in tabstop positions
  let g:UltiSnipsJumpForwardTrigger='<c-j>'
  let g:UltiSnipsJumpBackwardTrigger='<c-k>'

  " Configuration for custom snippets directory, see
  " https://jdhao.github.io/2019/04/17/neovim_snippet_s1/ for details.
  let g:UltiSnipsSnippetDirectories=['UltiSnips', 'my_snippets']
endif

"o--------------------------------------
"| wilder -> command line completion
"o--------------------------------------

if exists('g:vscode')
else
  call timer_start(250, { -> s:wilder_init() })

  function! s:wilder_init() abort
    try
      call wilder#setup({
            \ 'modes': [':', '/', '?'],
            \ 'next_key': '<Tab>',
            \ 'previous_key': '<S-Tab>',
            \ 'accept_key': '<C-y>',
            \ 'reject_key': '<C-e>'
            \ })

      call wilder#set_option('pipeline', [
            \   wilder#branch(
            \     wilder#cmdline_pipeline({
            \       'language': 'python',
            \       'fuzzy': 1,
            \       'sorter': wilder#python_difflib_sorter(),
            \       'debounce': 30,
            \     }),
            \     wilder#python_search_pipeline({
            \       'pattern': wilder#python_fuzzy_pattern(),
            \       'sorter': wilder#python_difflib_sorter(),
            \       'engine': 're',
            \       'debounce': 30,
            \     }),
            \   ),
            \ ])

      let l:hl = wilder#make_hl('WilderAccent', 'Pmenu', [{}, {}, {'foreground': '#f4468f'}])
      call wilder#set_option('renderer', wilder#popupmenu_renderer({
            \ 'highlighter': wilder#basic_highlighter(),
            \ 'max_height': 15,
            \ 'highlights': {
            \   'accent': l:hl,
            \ },
            \ 'left': [' ', wilder#popupmenu_devicons(),],
            \ 'right': [' ', wilder#popupmenu_scrollbar(),],
            \ 'apply_incsearch_fix': 0,
            \ }))
    catch /^Vim\%((\a\+)\)\=:E117/
      echohl Error |echomsg "Wilder.nvim missing"| echohl None
    endtry
  endfunction

endif

"o--------------------------------------
"| vim-commentary -> suppl. behaviour
"o--------------------------------------
if exists('g:vscode')
  " VSCode extension
  xmap gc  <Plug>VSCodeCommentary
  nmap gc  <Plug>VSCodeCommentary
  omap gc  <Plug>VSCodeCommentary
  nmap gcc <Plug>VSCodeCommentaryLine
else
  " ordinary Neovim
endif

if exists('g:vscode')
  " VSCode extension

  
else
  " ordinary Neovi

  " Do not use cache file
  let g:Lf_UseCache = 0
  
  " Refresh each time we call leaderf
  let g:Lf_UseMemoryCache = 0

  " Ignore certain files and directories when searching files
  let g:Lf_WildIgnore = {
    \ 'dir': ['.git', '__pycache__', '.DS_Store', '*_cache'],
    \ 'file': ['*.exe', '*.dll', '*.so', '*.o', '*.pyc', '*.jpg', '*.png',
    \ '*.gif', '*.svg', '*.ico', '*.db', '*.tgz', '*.tar.gz', '*.gz',
    \ '*.zip', '*.bin', '*.pptx', '*.xlsx', '*.docx', '*.pdf', '*.tmp',
    \ '*.wmv', '*.mkv', '*.mp4', '*.rmvb', '*.ttf', '*.ttc', '*.otf',
    \ '*.mp3', '*.aac']
    \}

  " Do not show fancy icons for Linux server.
  if g:is_linux
    let g:Lf_ShowDevIcons = 0
  endif

  " Only fuzzy-search files names
  let g:Lf_DefaultMode = 'FullPath'

  " Popup window settings
  let w = float2nr(&columns * 0.8)
  if w > 140
    let g:Lf_PopupWidth = 140
  else
    let g:Lf_PopupWidth = w
  endif

  let g:Lf_PopupPosition = [0, float2nr((&columns - g:Lf_PopupWidth)/2)]

  " Do not use version control tool to list files under a directory since
  " submodules are not searched by default.
  let g:Lf_UseVersionControlTool = 0

  " Use rg as the default search tool
  let g:Lf_DefaultExternalTool = "rg"

  " show dot files
  let g:Lf_ShowHidden = 1

  " Disable default mapping
  let g:Lf_ShortcutF = ''
  let g:Lf_ShortcutB = ''

  " set up working directory for git repository
  let g:Lf_WorkingDirectoryMode = 'a'

  " Search files in popup window
  nnoremap <silent> <leader>ff :<C-U>Leaderf file --popup<CR>

  " Grep project files in popup window
  nnoremap <silent> <leader>fg :<C-U>Leaderf rg --no-messages --popup<CR>

  " Search vim help files
  nnoremap <silent> <leader>fh :<C-U>Leaderf help --popup<CR>

  " Search tags in current buffer
  nnoremap <silent> <leader>ft :<C-U>Leaderf bufTag --popup<CR>

  " Switch buffers
  nnoremap <silent> <leader>fb :<C-U>Leaderf buffer --popup<CR>

  " Search recent files
  nnoremap <silent> <leader>fr :<C-U>Leaderf mru --popup --absolute-path<CR>

  let g:Lf_PopupColorscheme = 'catppuccin-latte'

  " Change keybinding in LeaderF prompt mode, use ctrl-n and ctrl-p to navigate
  " items.
  let g:Lf_CommandMap = {'<C-J>': ['<C-N>'], '<C-K>': ['<C-P>']}

  " do not preview results, it will add the file to buffer list
  let g:Lf_PreviewResult = {
    \ 'File': 0,
    \ 'Buffer': 0,
    \ 'Mru': 0,
    \ 'Tag': 0,
    \ 'BufTag': 1,
    \ 'Function': 1,
    \ 'Line': 0,
    \ 'Colorscheme': 0,
  \ 'Rg': 0,
  \ 'Gtags': 0
  \} 
endif
