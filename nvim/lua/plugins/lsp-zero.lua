local lsp_zero = require('lsp-zero')

lsp_zero.extend_lspconfig()

local wk = require("which-key")

lsp_zero.on_attach(function(client, bufnr)
  -- see :help lsp-zero-keybindings
  -- to learn the available actions
  lsp_zero.default_keymaps(
    {
      buffer = bufnr,
      preserve_mappings = false
    }
  )
  wk.register({
    v = {
      name = "LSP",
      -- code actions
      a = { function() vim.lsp.buf.code_action() end, "Code Action", noremap = true, buffer = bufnr },

      d = { function() vim.diagnostic.open_float() end, "Diagnostic", noremap = true, buffer = bufnr },
      r = { function() vim.lsp.buf.references() end, "References", noremap = true, buffer = bufnr },
      n = { function() vim.lsp.buf.rename() end, "Rename", noremap = true, buffer = bufnr },
      s = { function() vim.lsp.buf.workspace_symbol() end, "Workspace Symbol", noremap = true, buffer = bufnr },
    }

  }, { prefix = "<leader>" })
end)


require('mason').setup({})

require('mason-lspconfig').setup({
  -- Replace the language servers listed here
  -- with the ones you want to install
  ensure_installed = { 'rust_analyzer', 'clangd' },
  handlers = {
    lsp_zero.default_setup,
    clangd = function()
      require("lspconfig").clangd.setup {
        on_attach = lsp_zero.on_attach,
        capabilities = lsp_zero.capabilities,
        cmd = {
          "clangd",
          "--offset-encoding=utf-16",
        },
      }
    end,
  },
})
