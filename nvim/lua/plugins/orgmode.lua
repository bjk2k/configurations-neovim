require('orgmode').setup({
  org_agenda_files = { '~/personal/my-orgs/**/*' },
  org_default_notes_file = '~/personal/my-orgs/default/default_capture.org',
})
