local cmp = require('cmp')
local lspkind = require("lspkind")
local cmp_action = require('lsp-zero').cmp_action()

-- local has_words_before = function()
--   if vim.api.nvim_buf_get_option(0, "buftype") == "prompt" then return false end
--   local line, col = unpack(vim.api.nvim_win_get_cursor(0))
--   return col ~= 0 and vim.api.nvim_buf_get_text(0, line-1, 0, line-1, col, {})[1]:match("^%s*$") == nil
-- end

cmp.setup({
  sources = {
    -- lspzero sources
    { name = 'nvim_lsp' },
    { name = 'orgmode' },

    -- AI Source
    { name = "codeium" },
    { name = "copilot" },

    { name = "luasnip" },

  },
  mapping = cmp.mapping.preset.insert({
    -- next option
    ['<Tab>'] = cmp_action.luasnip_supertab(),
    ['<S-Tab>'] = cmp_action.luasnip_shift_supertab(),

    ['<CR>'] = cmp.mapping.confirm({ select = false }),

    -- Navigate between snippet placeholder
    ['<C-f>'] = cmp_action.luasnip_jump_forward(),
    ['<C-b>'] = cmp_action.luasnip_jump_backward(),

    -- Scroll up and down in the completion documentation
    ['<C-u>'] = cmp.mapping.scroll_docs(-4),
    ['<C-d>'] = cmp.mapping.scroll_docs(4),

    -- exit from completion
    ['<C-e>'] = cmp.mapping.close(),
  }),

  formatting = {
    format = lspkind.cmp_format({
      mode = "symbol",
      ellipsis_char = '...',
      max_width = 50,
      symbol_map = { Codeium = "", Copilot = "" }
    })
  },

  window = {
    completion = cmp.config.window.bordered(),
    documentation = cmp.config.window.bordered(),
  },
  sorting = {
    priority_weight = 2,
    comparators = {
      require("copilot_cmp.comparators").prioritize,

      -- Below is the default comparitor list and order for nvim-cmp
      cmp.config.compare.offset,
      -- cmp.config.compare.scopes, --this is commented in nvim-cmp too
      cmp.config.compare.exact,
      cmp.config.compare.score,
      cmp.config.compare.recently_used,
      cmp.config.compare.locality,
      cmp.config.compare.kind,
      cmp.config.compare.sort_text,
      cmp.config.compare.length,
      cmp.config.compare.order,
    },
  },
})
