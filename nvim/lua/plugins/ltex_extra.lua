require("ltex_extra").setup({
  init_check = true,
  -- string : relative or absolute path to store dictionaries
  -- e.g. subfolder in the project root or the current working directory: ".ltex"
  -- e.g. shared files for all projects:  vim.fn.expand("~") .. "/.local/share/ltex"
  path = vim.fn.expand("~") .. "/personal/dicts", -- project root or current working directory
})
