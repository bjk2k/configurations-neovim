require("yanky").setup({
  ring = {
    history_length = 50,
    storage = "memory",
  },
  preserve_cursor_position = {
    enabled = false,
  },
  picker = {
    select = {
      action = nil, -- nil to use default put action
    },
  },
})

-- cycle through the yank history, only work after paste

vim.keymap.set({"n","x"}, "p", "<Plug>(YankyPutAfter)")
vim.keymap.set({"n","x"}, "P", "<Plug>(YankyPutBefore)")
vim.keymap.set({"n","x"}, "gp", "<Plug>(YankyGPutAfter)")
vim.keymap.set({"n","x"}, "gP", "<Plug>(YankyGPutBefore)")

vim.keymap.set("n", "<c-n>", "<Plug>(YankyCycleForward)")
vim.keymap.set("n", "<c-p>", "<Plug>(YankyCycleBackward)")

require("telescope").load_extension("yank_history")
